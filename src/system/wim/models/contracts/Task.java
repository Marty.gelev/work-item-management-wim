package system.wim.models.contracts;

import system.wim.models.enums.Priority;

public interface Task extends WorkItem {
    Priority getPriority();

    Member getMemberAssignee();
}
