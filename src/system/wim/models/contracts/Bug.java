package system.wim.models.contracts;

import system.wim.models.enums.BugSeverity;
import system.wim.models.enums.BugStatus;
import system.wim.models.enums.Priority;

import javax.print.attribute.standard.Severity;

public interface Bug extends WorkItem, Task{
    String getStepsToReproduce();

    BugSeverity getSeverity();

    BugStatus getBugStatus();

    void changePriorityOfBug(Priority priority);

    void changeSeverityOfBug(BugSeverity severity);

    void changeStatusOfBug(BugStatus status);
}
