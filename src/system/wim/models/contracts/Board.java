package system.wim.models.contracts;

import java.util.List;

public interface Board {
    String getName();

    List<WorkItem> getWorkItems();

    void addToBoardActivityHistory(String activity);

    String getActivityHistory();

    void addWorkItem(WorkItem item);

}
