package system.wim.models.contracts;

public interface Comment {
    String getMessage();

    String getAuthor();
}
