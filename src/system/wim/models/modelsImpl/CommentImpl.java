package system.wim.models.modelsImpl;

import system.wim.models.contracts.Comment;

public class CommentImpl implements Comment {
    private static final String AUTHOR_CANNOT_BE_NULL_OR_EMPTY = "Author cannot be null or empty!";
    private static final String MESSAGE_CANNOT_BE_NULL_OR_EMPTY = "Message cannot be null or empty!";

    private String message;
    private String author;

    public CommentImpl(String author, String message) {
        setAuthor(author);
        setMessage(message);
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    public String toString() {
        return String.format("Message: %s Author: %s",
                getMessage(),
                getAuthor());
    }

    private void setAuthor(String author) {
        if (author == null || author.isEmpty()) {
            throw new IllegalArgumentException(AUTHOR_CANNOT_BE_NULL_OR_EMPTY);
        }
        this.author = author;
    }

    private void setMessage(String message) {
        if (message == null || message.isEmpty()) {
            throw new IllegalArgumentException(MESSAGE_CANNOT_BE_NULL_OR_EMPTY);
        }
        this.message = message;
    }
}
