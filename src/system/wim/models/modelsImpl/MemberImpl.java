package system.wim.models.modelsImpl;

import system.wim.models.contracts.Member;
import system.wim.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class MemberImpl implements Member {
    private static final Integer MEMBER_NAME_MIN_LENGTH = 5;
    private static final Integer MEMBER_NAME_MAX_LENGTH = 15;

    private static final String MEMBER_NAME_ERROR_MESSAGE = "The name of the member must be between %d and %d symbols!";
    private static final String EMPTY_NAME_MESSAGE = "Name cannot be null or empty!";

    private static final String MESSAGE_NULL_OR_EMPTY = "Message cannot be null or empty";

    private static final String WORK_ITEM_CANNOT_BE_NULL = "Work item cannot be null!";
    private static final String WORK_ITEM_NOT_FOUND_IN_BOARD ="Work item is not part of this Board";
    private static final String WORK_ITEM_EXIST_MESSAGE = "Work item already exist";

    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;

    public MemberImpl(String name) {
        setName(name);
        workItems = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    @Override
    public void addWorkItem(WorkItem workItem) {
        if (workItem == null) {
            throw new IllegalArgumentException(WORK_ITEM_CANNOT_BE_NULL);
        }
        if (workItems.contains(workItem)){
            throw new IllegalArgumentException(WORK_ITEM_EXIST_MESSAGE);
        }

        this.workItems.add(workItem);
    }

    @Override
    public void removeWorkItem(WorkItem workItem) {
        if (workItem == null) {
            throw new IllegalArgumentException(WORK_ITEM_CANNOT_BE_NULL);
        }
        if (!workItems.contains(workItem)){
            throw new NoSuchElementException(WORK_ITEM_NOT_FOUND_IN_BOARD);
        }

        this.workItems.remove(workItem);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getWorkItems() { return workItems; }

    @Override
    public List<String> getActivityHistory() { return activityHistory; }

    @Override
    public void addToActivityHistory(String message) {
        if (message==null || message.isEmpty()){
            throw new IllegalArgumentException(MESSAGE_NULL_OR_EMPTY);
        }
        activityHistory.add(message);
    }

    private void setName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_NAME_MESSAGE);
        }
        if (name.length() < MEMBER_NAME_MIN_LENGTH || name.length() > MEMBER_NAME_MAX_LENGTH) {
            throw new IllegalArgumentException(String.format(MEMBER_NAME_ERROR_MESSAGE, MEMBER_NAME_MIN_LENGTH, MEMBER_NAME_MAX_LENGTH));
        }

        this.name = name;
    }
}
