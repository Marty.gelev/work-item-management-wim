package system.wim.models.modelsImpl;

import system.wim.models.contracts.History;
import system.wim.models.contracts.Member;
import system.wim.models.contracts.Story;
import system.wim.models.enums.Priority;
import system.wim.models.enums.StorySize;
import system.wim.models.enums.StoryStatus;

public class StoryImpl extends TaskImpl implements Story {
    private final static String WORK_ITEM_TYPE = "Story ----";

    private StorySize size;
    private StoryStatus status;

    public StoryImpl(String title, String description, Priority priority, Member memberAssignee, StorySize size, StoryStatus status) {
        super(title, description, priority, memberAssignee);
        setStorySize(size);
        setStoryStatus(status);
    }

    @Override
    public void changeStoryPriority(Priority priority) {
        History history = new HistoryImpl("story priority", getPriority().toString(), priority.toString());
        super.addHistory(history);
        setPriority(priority);
    }

    @Override
    public void changeStoryStatus(StoryStatus status) {
        History history = new HistoryImpl("story status", getStoryStatus().toString(), status.toString());
        super.addHistory(history);
        setStoryStatus(status);
    }

    @Override
    public void changeStorySize(StorySize size) {
        History history = new HistoryImpl("story size", getStoryStatus().toString(), size.toString());
        super.addHistory(history);
        setStorySize(size);
    }

    @Override
    public StorySize getStorySize() {
        return this.size;
    }

    @Override
    public StoryStatus getStoryStatus() {
        return this.status;
    }

    @Override
    public String printWorkItemType() {
        return WORK_ITEM_TYPE;
    }

    protected String printDetails() {
        StringBuilder sb = new StringBuilder(super.printDetails());
        sb.append("Story size: ").append(getStorySize()).append(System.lineSeparator())
                .append("Story status: ").append(getStoryStatus().toString())
                .append(System.lineSeparator()).append("---------------------").append(System.lineSeparator());
        return sb.toString();
    }

    @Override
    protected void setPriority(Priority priority) {
        super.setPriority(priority);
    }

    private void setStorySize(StorySize size) {
        this.size = size;
    }

    private void setStoryStatus(StoryStatus status) {
        this.status = status;
    }
}
