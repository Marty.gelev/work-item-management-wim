package system.wim.models.modelsImpl;

import system.wim.models.contracts.Feedback;
import system.wim.models.contracts.History;
import system.wim.models.enums.FeedBackStatus;

public class FeedbackImpl extends WorkItemImpl implements Feedback {
    private final static String WORK_ITEM_TYPE = "Feedback ----";
    private static final String RATING_LESS_THAN_ZERO_MESSAGE = "Rating must be greater than zero!";
    private static final String FEEDBACK_STATUS_NULL_MESSAGE = "Feedback status cannot be null!";

    private int rating;
    private FeedBackStatus status;

    public FeedbackImpl(String title, String description, int rating, FeedBackStatus status) {
        super(title, description);
        setFeedbackRating(rating);
        setFeedbackStatus(status);
    }

    @Override
    public void changeFeedbackStatus(FeedBackStatus status) {
        History history = new HistoryImpl("feedback status", getFeedbackStatus().toString(), status.toString());
        super.addHistory(history);
        setFeedbackStatus(status);
    }

    @Override
    public void changeFeedbackRating(int rating) {
        History history = new HistoryImpl("feedback rating", String.valueOf(getFeedbackRating()), String.valueOf(rating));
        super.addHistory(history);
        setFeedbackRating(rating);
    }

    @Override
    public int getFeedbackRating() {
        return this.rating;
    }

    @Override
    public FeedBackStatus getFeedbackStatus() {
        return this.status;
    }

    @Override
    public String printWorkItemType() {
        return WORK_ITEM_TYPE;
    }

    protected String printDetails() {
        StringBuilder sb = new StringBuilder(super.printDetails());
        sb.append("Feedback rating: ").append(getFeedbackRating())
                .append(System.lineSeparator())
                .append("Feedback status: ").append(getFeedbackStatus().toString())
                .append(System.lineSeparator())
        .append("***********************").append(System.lineSeparator());

        return sb.toString();
    }

    private void setFeedbackRating(int rating) {
        if (rating <= 0) {
            throw new IllegalArgumentException(RATING_LESS_THAN_ZERO_MESSAGE);
        }
        this.rating = rating;
    }

    private void setFeedbackStatus(FeedBackStatus status) {
        if (status==null){
            throw new NullPointerException(FEEDBACK_STATUS_NULL_MESSAGE);
        }
        this.status = status;
    }
}
