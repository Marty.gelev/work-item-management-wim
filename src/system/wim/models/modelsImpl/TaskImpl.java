package system.wim.models.modelsImpl;

import system.wim.models.contracts.Member;
import system.wim.models.contracts.Task;
import system.wim.models.enums.Priority;

public class TaskImpl extends WorkItemImpl implements Task {
    private static final String MEMBER_ASSIGNEE_CANNOT_BE_NULL = "Member Assignee cannot be null!";

    private Priority priority;
    private Member memberAssignee;

    public TaskImpl(String title, String description, Priority priority, Member memberAssignee) {
        super(title, description);
        setPriority(priority);
        setMemberAssignee(memberAssignee);
    }

    public Priority getPriority() {
        return priority;
    }

    public Member getMemberAssignee() {
        return memberAssignee;
    }

    protected String printDetails(){
        StringBuilder sb = new StringBuilder(super.printDetails());
        sb.append("Priority: ")
                .append(getPriority())
                .append(System.lineSeparator())
                .append("Member Assignee: ")
                .append(getMemberAssignee().getName())
                .append(System.lineSeparator());
        return sb.toString();
    }

    protected void setPriority(Priority priority) {
        this.priority = priority;
    }

    private void setMemberAssignee(Member memberAssignee) {
        if (memberAssignee == null) {
            throw new IllegalArgumentException(MEMBER_ASSIGNEE_CANNOT_BE_NULL);
        }
        this.memberAssignee = memberAssignee;
    }
}
