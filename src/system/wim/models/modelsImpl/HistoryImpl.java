package system.wim.models.modelsImpl;

import system.wim.models.contracts.History;

public class HistoryImpl implements History {
    private static final String CHANGED_PROPERTY_CANNOT_BE_NULL_OR_EMPTY = "Changed property cannot be null or empty!";
    private static final String OLD_VALUE_CANNOT_BE_NULL_OR_EMPTY = "Old value cannot be null or empty!";
    private static final String NEW_VALUE_CANNOT_BE_NULL_OR_EMPTY = "New value cannot be null or empty!";

    private String changedProperty;
    private String oldValue;
    private String newValue;

    public HistoryImpl(String changedProperty, String oldValue, String newValue) {
        setChangedProperty(changedProperty);
        setOldValue(oldValue);
        setNewValue(newValue);
    }

    public String getChangedProperty() {
        return changedProperty;
    }

    public String getOldValue() {
        return oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    @Override
    public String toString() {
        return String.format("The value for %s was changed. The old value was %s, and the new value is %s",
                getChangedProperty(),
                getOldValue(),
                getNewValue());
    }

    private void setChangedProperty(String changedProperty) {
        if (changedProperty == null || changedProperty.isEmpty()) {
            throw new IllegalArgumentException(CHANGED_PROPERTY_CANNOT_BE_NULL_OR_EMPTY);
        }
        this.changedProperty = changedProperty;
    }

    private void setNewValue(String newValue) {
        if (newValue == null || newValue.isEmpty()) {
            throw new IllegalArgumentException(NEW_VALUE_CANNOT_BE_NULL_OR_EMPTY);
        }
        this.newValue = newValue;
    }

    private void setOldValue(String oldValue) {
        if (oldValue == null || oldValue.isEmpty()) {
            throw new IllegalArgumentException(OLD_VALUE_CANNOT_BE_NULL_OR_EMPTY);
        }
        this.oldValue = oldValue;
    }
}
