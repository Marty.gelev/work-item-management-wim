package system.wim.models.modelsImpl;

import system.wim.models.contracts.Comment;
import system.wim.models.contracts.History;
import system.wim.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class WorkItemImpl implements WorkItem {
    private static final Integer TITLE_MIN_LENGTH = 10;
    private static final Integer TITLE_MAX_LENGTH = 50;

    private static final Integer DESCRIPTION_MIN_LENGTH = 10;
    private static final Integer DESCRIPTION_MAX_LENGTH = 500;

    private static final String TITLE_LENGTH_ERROR_MESSAGE = "Title length must be between %d and %d symbols!";
    private static final String TITLE_NULL_OR_EMPTY_MESSAGE = "Title cannot be null or empty!";

    private static final String DESCRIPTION_LENGTH_ERROR_MESSAGE = "Description length must be between %d and %d symbols!";
    private static final String DESCRIPTION_NULL_OR_EMPTY_MESSAGE = "Description cannot be null or empty!";

    private static final String COMMENT_IS_NULL_MESSAGE = "Comment cannot be null";
    private static final String COMMENT_LIST_EMPTY_MESSAGE = "No comments found";

    private static final String HISTORY_CANNOT_BE_NULL = "History cannot be null!";
    private static final String HISTORY_LIST_EMPTY_MESSAGE = "No History found!";

    private static int lastUsedId = 0;
    private int id;
    private String title;
    private String description;
    private List<Comment> commentList;
    private List<History> historyList;

     WorkItemImpl(String title, String description) {
        id =generateNextId();
        setTitle(title);
        setDescription(description);
        commentList = new ArrayList<>();
        historyList = new ArrayList<>();
    }

    public void addComment(Comment comment) {
        if (comment == null) {
            throw new IllegalArgumentException(COMMENT_IS_NULL_MESSAGE);
        }
        this.commentList.add(comment);
    }

    public void addHistory(History history) {
        if (history == null) {
            throw new IllegalArgumentException(HISTORY_CANNOT_BE_NULL);
        }
        this.historyList.add(history);
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getComments() {
        if (commentList.isEmpty()){
            throw new NoSuchElementException(COMMENT_LIST_EMPTY_MESSAGE);
        }
         return commentList.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    @Override
    public String getHistory() {
        if (historyList.isEmpty()){
            throw new NoSuchElementException(HISTORY_LIST_EMPTY_MESSAGE);
        }
         return historyList.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    public String printWorkItemType() {
        return "";
    }

    public String toString() {
        return print();
    }

    protected String printDetails() {
        return String.format(
                "Item Id: %d" + System.lineSeparator() +
                        "Title: %s" + System.lineSeparator() +
                        "Description: %s" + System.lineSeparator(), getId(), getTitle(), getDescription());

    }

    private void setDescription(String description) {
        if (description==null || description.isEmpty()){
            throw new IllegalArgumentException(DESCRIPTION_NULL_OR_EMPTY_MESSAGE);
        }
        if (description.length() < DESCRIPTION_MIN_LENGTH || description.length() > DESCRIPTION_MAX_LENGTH) {
            throw new IllegalArgumentException(String.format(DESCRIPTION_LENGTH_ERROR_MESSAGE, DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH));
        }
        this.description = description;
    }

    private void setTitle(String title) {
        if (title==null || title.isEmpty())
        {
            throw new IllegalArgumentException(TITLE_NULL_OR_EMPTY_MESSAGE);
        }
        if (title.length() < TITLE_MIN_LENGTH || title.length() > TITLE_MAX_LENGTH) {
            throw new IllegalArgumentException(String.format(TITLE_LENGTH_ERROR_MESSAGE, TITLE_MIN_LENGTH, TITLE_MAX_LENGTH));
        }
        this.title = title;
    }

    private String print() {
        StringBuilder sb = new StringBuilder();
        sb.append(printWorkItemType());
        sb.append(System.lineSeparator());
        sb.append(printDetails());
        return sb.toString();
    }

    private int generateNextId() {
        return lastUsedId++;
    }
}
