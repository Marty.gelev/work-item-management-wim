package system.wim.commands.filterCommand;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Bug;
import system.wim.models.contracts.Feedback;
import system.wim.models.contracts.Story;

import java.util.List;

public class FilterByStatusCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse status parameters";

    private final WorkItemFactory factory;
    private final Engine engine;

    public FilterByStatusCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String status;

        try {
            status = parameters.get(0);
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        System.out.println("Filter by status:");
        filterStatus(status);
        return "";
    }

    private void filterStatus(String status){
        for (Bug value : engine.getAllBugs().values()) {
            if (value.getBugStatus().toString().equals(status)) {
                System.out.println(value);
                System.out.println("#########################");
            }
        }

        for (Story value : engine.getAllStories().values()) {
            if (value.getStoryStatus().toString().equals(status)) {
                System.out.println(value);
                System.out.println("#########################");
            }
        }

        for (Feedback value : engine.getAllFeedbacks().values()) {
            if (value.getFeedbackStatus().toString().equals(status)) {
                System.out.println(value);
                System.out.println("#########################");
            }
        }
    }
}