package system.wim.commands.filterCommand;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Feedback;

import java.util.List;
import java.util.stream.Collectors;

public class FilterFeedbackCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse board name parameter";
    private final static String NO_BOARD_MESSAGE = "Board with name %s does not exist";
    private final static String NO_FEEDBACK_FOUND_MESSAGE = "No feedback in this board";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public FilterFeedbackCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String boardName ;

        try{
            boardName=parameters.get(0);
        } catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllBoards().containsKey(boardName)){
            throw new IllegalArgumentException(String.format(NO_BOARD_MESSAGE,boardName));
        }

        //find better solution
         long count = engine.getAllBoards()
        .get(boardName)
        .getWorkItems()
        .stream()
        .filter(x->x instanceof Feedback)
        .map(x->(Feedback)x).count();

        if (count==0){
            throw new IllegalArgumentException(NO_FEEDBACK_FOUND_MESSAGE);
        }

        System.out.println("Filter feedback:");
   return engine.getAllBoards()
           .get(boardName)
           .getWorkItems()
           .stream()
           .filter(x->x instanceof Feedback)
           .map(x->(Feedback)x)
           .collect(Collectors.toList())
           .toString()
           .replace("[","")
           .replace("]","");
    }
}
