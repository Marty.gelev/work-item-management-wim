package system.wim.commands.filterCommand;
import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Bug;
import system.wim.models.contracts.Story;

import java.util.List;
    public class FilterByAssigneeCommand implements Command {
        private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse assignee parameter";
        private static final String NO_PERSON_FOUND = "No such person found";

        private final WorkItemFactory factory;
        private final Engine engine;

        public FilterByAssigneeCommand(WorkItemFactory factory, Engine engine) {
            this.factory = factory;
            this.engine = engine;
        }

        @Override
        public String execute(List<String> parameters) {
            String assignee;

            try {
                assignee = parameters.get(0);
            } catch (Exception ex) {
                throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
            }

            if (!engine.getAllMembers().containsKey(assignee)){
                throw new IllegalArgumentException(NO_PERSON_FOUND);
            }

            System.out.println("Filter by assignee:");
            filterByAssignee(assignee);
            return "";
        }

        private void filterByAssignee(String assignee) {
            for (Bug value : engine.getAllBugs().values()) {
                if (value.getMemberAssignee().getName().equals(assignee)) {
                    System.out.println(value);
                    System.out.println("#########################");
                }
            }

            for (Story value : engine.getAllStories().values()) {
                if (value.getMemberAssignee().getName().equals(assignee)) {
                    System.out.println(value);
                    System.out.println("#########################");
                }
            }
        }
    }



