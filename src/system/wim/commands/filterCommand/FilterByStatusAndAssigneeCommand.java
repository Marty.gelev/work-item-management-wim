package system.wim.commands.filterCommand;
import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Bug;
import system.wim.models.contracts.Story;

import java.util.List;

public class FilterByStatusAndAssigneeCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse assignee or status parameter";
    private static final String NO_PERSON_FOUND = "No such person found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public FilterByStatusAndAssigneeCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String status;
        String assignee;

        try {
            status = parameters.get(0);
            assignee = parameters.get(1);
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllMembers().containsKey(assignee)){
            throw new IllegalArgumentException(NO_PERSON_FOUND);
        }

        System.out.println("Filter by status and assignee:");
        filterByStatusAndAssignee(status,assignee);
        return "";
    }

    private void filterByStatusAndAssignee(String status, String assignee) {
        for (Bug value : engine.getAllBugs().values()) {
            if (value.getBugStatus().toString().equals(status) && value.getMemberAssignee().getName().equals(assignee)) {
                System.out.println(value);
                System.out.println("#########################");
            }
        }

        for (Story value : engine.getAllStories().values()) {
            if (value.getStoryStatus().toString().equals(status) && value.getMemberAssignee().getName().equals(assignee)) {
                System.out.println(value);
                System.out.println("#########################");
            }
        }
    }
}
