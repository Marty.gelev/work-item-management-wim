package system.wim.commands.filterCommand;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Bug;
import java.util.List;
import java.util.stream.Collectors;

public class FilterBugsCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse board parameters";
    private static final String NO_BUGS_FOUND_MESSAGE = "No bugs in this board";
    private final WorkItemFactory factory;
    private final Engine engine;

    public FilterBugsCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }
 //TODO
    @Override
    public String execute(List<String> parameters) {
        String boardName;

        try {
            boardName = parameters.get(0);
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        //find better solution
        long count = engine.getAllBoards()
                .get(boardName)
                .getWorkItems()
                .stream()
                .filter(x -> x instanceof Bug)
                .map(x -> (Bug) x).count();

        if (count==0){
            throw new IllegalArgumentException(NO_BUGS_FOUND_MESSAGE);
        }

        System.out.println("Filter bugs:");
        return engine.getAllBoards()
                .get(boardName)
                .getWorkItems()
                .stream()
                .filter(x -> x instanceof Bug)
                .map(x -> (Bug) x)
                .collect(Collectors.toList())
                .toString()
                .replace("[", "")
                .replace("]", "");
    }
}
