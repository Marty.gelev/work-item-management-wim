package system.wim.commands.sortCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortByTitleCommand implements Command {
    private final WorkItemFactory factory ;
    private final Engine engine;

    public SortByTitleCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        System.out.println("&&&&&&&&&&&&&&&&&&&&");
        System.out.println("Sorting by title:");
        List<WorkItem> items = new ArrayList<>(engine.getAllWorkItems().values());
        items.sort(Comparator.comparing(WorkItem::getTitle));
        items.forEach(System.out::println);

        return "&&&&&&&&&&&&&&&&&&&&";
    }
}
