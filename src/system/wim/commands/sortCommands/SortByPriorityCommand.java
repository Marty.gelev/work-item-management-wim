package system.wim.commands.sortCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Task;

import java.util.Comparator;
import java.util.List;

public class SortByPriorityCommand implements Command {
    private final WorkItemFactory factory ;
    private final Engine engine;

    public SortByPriorityCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        System.out.println("++++++++++++++++++++++++++++");
        System.out.println("Sorting by priority:");
        engine.getAllTasks().sort(Comparator.comparing(Task::getPriority));
        engine.getAllTasks().forEach(System.out::println);

        return "++++++++++++++++++++++++++++";
    }
}
