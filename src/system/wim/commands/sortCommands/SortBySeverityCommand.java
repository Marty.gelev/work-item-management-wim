package system.wim.commands.sortCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Bug;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortBySeverityCommand implements Command {
    private final WorkItemFactory factory;
    private final Engine engine;

    public SortBySeverityCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        System.out.println("*********************");
        System.out.println("Sorting by severity:");
        List<Bug> bugs = new ArrayList<>(engine.getAllBugs().values());
                bugs.sort(Comparator.comparing(Bug::getSeverity));
                bugs.forEach(System.out::println);

        return "*********************";
    }
}


