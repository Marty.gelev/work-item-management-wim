package system.wim.commands.sortCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Story;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortBySizeCommand implements Command {
    private final WorkItemFactory factory;
    private final Engine engine;

    public SortBySizeCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        System.out.println("$$$$$$$$$$$$$$$$$$$$");
        System.out.println("Sorting by size:");
        List<Story> stories = new ArrayList<>(engine.getAllStories().values());
        stories.sort(Comparator.comparing(Story::getStorySize));
        stories.forEach(System.out::println);
        return "$$$$$$$$$$$$$$$$$$$$";
    }
}
