package system.wim.commands.changeCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import java.util.List;
import java.util.NoSuchElementException;

public class ChangeRatingOfFeedbackCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to change Feedback rating parameters!";
    private static final String FEEDBACK_NOT_FOUND = "Feedback %s not found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public ChangeRatingOfFeedbackCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String feedbackName;
        int newRating;

        try {
            feedbackName = parameters.get(0);
            newRating = Integer.parseInt(parameters.get(1));
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllFeedbacks().containsKey(feedbackName)){
            throw new NoSuchElementException(String.format(FEEDBACK_NOT_FOUND,feedbackName));
        }

        engine.getAllFeedbacks().get(feedbackName).changeFeedbackRating(newRating);
        return String.format("Rating of Feedback was changed to %s", newRating);
    }
}
