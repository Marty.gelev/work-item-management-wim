package system.wim.commands.changeCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.enums.BugStatus;

import java.util.List;
import java.util.NoSuchElementException;

public class ChangeStatusOfBugCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse bug status parameters!";
    private static final String BUG_NOT_FOUND_MESSAGE = "Bug not found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public ChangeStatusOfBugCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String bugName;
        BugStatus newStatus;

        try {
            bugName = parameters.get(0);
            newStatus = BugStatus.valueOf(parameters.get(1).toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if(!engine.getAllBugs().containsKey(bugName)){
            throw new NoSuchElementException(BUG_NOT_FOUND_MESSAGE);
        }

        engine.getAllBugs().get(bugName).changeStatusOfBug(newStatus);
        return String.format("Status of bug was changed to %s", newStatus);
    }
}
