package system.wim.commands.changeCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Story;
import system.wim.models.contracts.WorkItem;
import system.wim.models.enums.Priority;

import java.util.List;
import java.util.NoSuchElementException;

public class ChangePriorityOfStoryCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS ="[ERROR]: Failed to parse story parameters!";
    private static final String STORY_NOT_FOUND ="Story %s not found!";

    private final WorkItemFactory factory;
    private final Engine engine;

    public ChangePriorityOfStoryCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }
    @Override
    public String execute(List<String> parameters) {
        String story;
        Priority newPriority;

        try {
            story =  parameters.get(0);
            newPriority = Priority.valueOf(parameters.get(1).toUpperCase());
        }catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllStories().containsKey(story)){
            throw new NoSuchElementException(String.format(STORY_NOT_FOUND,story));
        }

        engine.getAllStories().get(story).changeStoryPriority(newPriority);
        return String.format("The priority of story was changed to %s",newPriority);
    }

}
