package system.wim.commands.changeCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.enums.StorySize;

import java.util.List;
import java.util.NoSuchElementException;

public class ChangeSizeOfStoryCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse story parameters!";
    private static final String STORY_NOT_FOUND ="Story %s not found!";

    private final WorkItemFactory factory;
    private final Engine engine;

    public ChangeSizeOfStoryCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public String execute(List<String> parameters) {
        String storyName;
        StorySize newStorySize;

        try {
            storyName = parameters.get(0);
            newStorySize = StorySize.valueOf(parameters.get(1).toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllStories().containsKey(storyName)){
            throw new NoSuchElementException(String.format(STORY_NOT_FOUND,storyName));
        }

        engine.getAllStories().get(storyName).changeStorySize(newStorySize);
        return String.format("The size of story was changed to %s", newStorySize);
    }
}


