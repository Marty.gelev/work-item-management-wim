package system.wim.commands.changeCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.enums.Priority;

import java.util.List;

public class ChangePriorityOfBugCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse bug parameters!";
    private static final String BUG_NOT_FOUND ="Bug not found";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public ChangePriorityOfBugCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String bugName;
        Priority newPriority;

        try {
            bugName = parameters.get(0);
            newPriority = Priority.valueOf(parameters.get(1).toUpperCase());
        }catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if(!engine.getAllBugs().containsKey(bugName)){
            throw new IllegalArgumentException(BUG_NOT_FOUND);
        }

        engine.getAllBugs().get(bugName).changePriorityOfBug(newPriority);
        return String.format("Priority type of Bug changed to : %s",newPriority);
    }
}
