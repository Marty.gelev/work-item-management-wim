package system.wim.commands.changeCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.enums.FeedBackStatus;

import java.util.List;
import java.util.NoSuchElementException;

public class ChangeStatusOfFeedbackCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse feedback status parameter!";
    private static final String FEEDBACK_NOT_FOUND = "Feedback %s not found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public ChangeStatusOfFeedbackCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String feedbackName;
        FeedBackStatus newStatus;

        try {
            feedbackName =  parameters.get(0);
            newStatus = FeedBackStatus.valueOf(parameters.get(1).toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllFeedbacks().containsKey(feedbackName)){
            throw new NoSuchElementException(String.format(FEEDBACK_NOT_FOUND,feedbackName));
        }

        engine.getAllFeedbacks().get(feedbackName).changeFeedbackStatus(newStatus);
        return String.format("Status of Feedback was changed to %s", newStatus);
    }
}
