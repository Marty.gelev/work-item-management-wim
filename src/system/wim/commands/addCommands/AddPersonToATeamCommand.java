package system.wim.commands.addCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Member;

import java.util.List;
import java.util.NoSuchElementException;

public class AddPersonToATeamCommand implements Command {
    private final static String MEMBER_ALREADY_EXIST_MESSAGE = "Member with name %s already exist in team %s";
    private final static String MEMBER_NOT_FOUND = "Member %s not found";

    private final static String FAILED_TO_PARSE_PARAMETERS = "[Error]: Failed to parse parameters for addPersonToATeam command";

    private final static String TEAM_NOT_FOUND = "Team %s not found";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public AddPersonToATeamCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String personName;
        String teamName;

        try{
            personName = parameters.get(0);
            teamName = parameters.get(1);
        }catch (Exception e){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllTeams().containsKey(teamName)){
            throw new NoSuchElementException(String.format(TEAM_NOT_FOUND,teamName));
        }

        if (!engine.getAllMembers().containsKey(personName)){
            throw new NoSuchElementException(String.format(MEMBER_NOT_FOUND,personName));
        }

        Member member = engine.getAllMembers().get(personName);

        if (engine.getAllTeams().get(teamName).getMembers().containsKey(personName)){
            throw new IllegalArgumentException(String.format(MEMBER_ALREADY_EXIST_MESSAGE,personName,teamName));
        }

        engine.getAllMembers().put(personName,member);
        engine.getAllTeams().get(teamName).addMember(personName, member);

        String message = String.format("%s added to %s team",personName,teamName);
        engine.getAllTeams().get(teamName).addToTeamActivityHistory(message);
        engine.getAllMembers().get(personName).addToActivityHistory(message);
        return message;
    }
}
