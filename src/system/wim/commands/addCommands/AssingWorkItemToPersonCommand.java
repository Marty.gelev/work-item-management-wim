package system.wim.commands.addCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Member;
import system.wim.models.contracts.WorkItem;

import java.util.List;
import java.util.NoSuchElementException;

public class AssingWorkItemToPersonCommand implements Command {
    private final static String FAILED_TO_PARSE_PARAMETERS_MESSAGE = "[Error]: Failed to parse parameters while adding work item to person!";

    private final static String MEMBER_ALREADY_HAVE_WORK_ITEM_MESSAGE = "Member %s already assigned for this work item %s";
    private final static String MEMBER_NOT_FOUND ="Member %s not found";

    private final static String WORK_ITEM_NOT_FOUND ="Work item not found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public AssingWorkItemToPersonCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String workItem;
        String personName;

        try {
            workItem = parameters.get(0);
            personName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS_MESSAGE);
        }

        if (!engine.getAllMembers().containsKey(personName)){
            throw new NoSuchElementException(String.format(MEMBER_NOT_FOUND,personName));
        }

        if (!engine.getAllWorkItems().containsKey(workItem)){
            throw new NoSuchElementException(WORK_ITEM_NOT_FOUND);
        }

        WorkItem item = engine.getAllWorkItems().get(workItem);
        Member member = engine.getAllMembers().get(personName);

        if (member.getWorkItems().contains(item)){
            throw new IllegalArgumentException(String.format(MEMBER_ALREADY_HAVE_WORK_ITEM_MESSAGE,personName,workItem));
        }

        member.addWorkItem(item);
        String message = String.format("Work item : %s added to person %s", workItem, personName);
        member.addToActivityHistory(message);
        return message;
    }
}
