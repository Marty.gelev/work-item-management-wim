package system.wim.commands.addCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Comment;
import system.wim.models.contracts.WorkItem;
import system.wim.models.modelsImpl.CommentImpl;

import java.util.List;
import java.util.NoSuchElementException;

public class AddCommentToWorkItemCommand implements Command {
    private static final String WORK_ITEM_NOT_FOUND = "Work Item not found";
    private static final String PERSON_NOT_FOUND = "Person not found";

    private static final String FAILED_TO_PARSE_PARAMETERS_ERROR = "[Error]: Failed to parse comment parameters!";

    private final WorkItemFactory factory;
    private final Engine engine;

    public AddCommentToWorkItemCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String workItemName;
        String commentAuthor;
        String commentMessage;

        try {
            workItemName = parameters.get(0);
            commentAuthor = parameters.get(1);
            commentMessage = parameters.get(2);
        } catch (Exception e) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS_ERROR);
        }

        if (!engine.getAllWorkItems().containsKey(workItemName)) {
            throw new NoSuchElementException(WORK_ITEM_NOT_FOUND);
        }

        if (!engine.getAllMembers().containsKey(commentAuthor)){
            throw new NoSuchElementException(PERSON_NOT_FOUND);
        }

        WorkItem workItem = engine.getAllWorkItems().get(workItemName);
        Comment comment = new CommentImpl(commentAuthor, commentMessage);
        workItem.addComment(comment);

        String message = String.format("A new comment was added to work item %s. The Author is: %s and the message is: \"%s\"", workItemName, comment.getAuthor(), comment.getMessage());
        engine.getAllMembers().get(commentAuthor).addToActivityHistory(message);
        return message;
    }
}
