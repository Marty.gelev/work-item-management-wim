package system.wim.commands.showCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import java.util.List;
import java.util.NoSuchElementException;

public class ShowAllPeopleCommand implements Command {
    private static final String NO_MEMBERS_FOUND_MESSAGE = "No members found";
    private final WorkItemFactory factory ;
    private final Engine engine;

    public ShowAllPeopleCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }
    @Override
    public String execute(List<String> parameters) {
        String result;

            if (engine.getAllMembers().keySet().isEmpty()){
                throw new NoSuchElementException(NO_MEMBERS_FOUND_MESSAGE);
            }

        System.out.print("All people: ");
          result = engine.getAllMembers().keySet().toString().replace("[","").replace("]","");

        return result;
    }
}
