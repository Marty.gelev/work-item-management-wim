package system.wim.commands.showCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.WorkItem;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ListAllCommand implements Command {

    private static final String NO_ITEMS_TO_LIST_MESSAGE = "No items to list!";
    private static final String FAILED_TO_SHOW_ALL_ITEMS_MESSAGE = "[ERROR]: Failed to show all items!";
    private final WorkItemFactory factory;
    private final Engine engine;

    public ListAllCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String result;

        try {
            if (engine.getAllWorkItems().keySet().toString().isEmpty()) {
                throw new IllegalArgumentException(NO_ITEMS_TO_LIST_MESSAGE);
            }
            Collection<WorkItem> values = engine.getAllWorkItems().values();
            result = values.stream().map(Object::toString).collect(Collectors.joining(""));
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_SHOW_ALL_ITEMS_MESSAGE);
        }

        return result;
    }
}
