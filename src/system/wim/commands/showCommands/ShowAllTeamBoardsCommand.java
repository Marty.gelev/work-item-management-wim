package system.wim.commands.showCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import java.util.List;
import java.util.NoSuchElementException;

public class ShowAllTeamBoardsCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse team's name parameter";
    private static final String NO_BOARDS_FOUND = "No boards found in %s team";
    private final static String TEAM_NOT_FOUND = "Team %s not found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public ShowAllTeamBoardsCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }
    @Override
    public String execute(List<String> parameters) {
        String teamName;

        try{
            teamName =parameters.get(0);
        }
        catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllTeams().containsKey(teamName)){
            throw new NoSuchElementException(String.format(TEAM_NOT_FOUND,teamName));
        }

        if (engine.getAllTeams().get(teamName).getBoards().isEmpty()){
            throw new IllegalArgumentException(String.format(NO_BOARDS_FOUND,teamName));
        }

        System.out.print(String.format("All team (%s) boards: ",teamName));
       return engine.getAllTeams()
               .get(teamName)
               .getBoards()
               .keySet()
               .toString()
               .replace("[","")
               .replace("]","");

    }
}
