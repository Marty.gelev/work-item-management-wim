package system.wim.commands.showCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;

import java.util.List;
import java.util.NoSuchElementException;

public class ShowAllTeamMembersCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse team's name parameter";
    private static final String TEAM_NOT_FOUND_MESSAGE = "Team not found";
    private static final String NO_MEMBERS_FOUND_MESSAGE = "No members found in this team (%s)";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public ShowAllTeamMembersCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }
    @Override
    public String execute(List<String> parameters) {
        String teamName;

        try{
            teamName = parameters.get(0);
        } catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllTeams().containsKey(teamName)){
            throw new NoSuchElementException(TEAM_NOT_FOUND_MESSAGE);
        }

        if (engine.getAllTeams().get(teamName).showAllTeamMembers().isEmpty()){
            throw new NoSuchElementException(String.format(NO_MEMBERS_FOUND_MESSAGE,teamName));
        }

        System.out.print(String.format("All team (%s) members: ",teamName));
        return engine.getAllTeams().get(teamName).showAllTeamMembers();
    }
}
