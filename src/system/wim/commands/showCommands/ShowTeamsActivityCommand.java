package system.wim.commands.showCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;

import java.util.List;
import java.util.NoSuchElementException;

public class ShowTeamsActivityCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse team's name parameter!";
    private static final String NO_TEAM_FOUND = "Team %s not found";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public ShowTeamsActivityCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }
    @Override
    public String execute(List<String> parameters) {
        String name;
        String result= "Team Activity: ";

        try{
            name = parameters.get(0);
        }catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllTeams().containsKey(name)){
            throw new NoSuchElementException(String.format(NO_TEAM_FOUND,name));
        }

        if (engine.getAllTeams().containsKey(name)){
         result+= engine.getAllTeams().get(name).showTeamActivity(name);
        }

        return result;
    }
}
