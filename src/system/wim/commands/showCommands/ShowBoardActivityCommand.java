package system.wim.commands.showCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;

import java.util.List;

public class ShowBoardActivityCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR] Failed to parse board name parameter";
    private static final String BOARD_NOT_FOUND_MESSAGE = "Board not found";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public ShowBoardActivityCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }
    @Override
    public String execute(List<String> parameters) {
        String boardName;

        try {
            boardName = parameters.get(0);
        }catch (Exception e){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllBoards().containsKey(boardName)){
            throw new IllegalArgumentException(BOARD_NOT_FOUND_MESSAGE);
        }

        System.out.print(String.format("Board's (%s) activity history",boardName));
        return engine.getAllBoards().get(boardName).getActivityHistory();
    }
}
