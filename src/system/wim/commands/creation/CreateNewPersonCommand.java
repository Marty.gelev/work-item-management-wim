package system.wim.commands.creation;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Member;
import java.util.List;

public class CreateNewPersonCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse Person's parameters";
    private static final String PERSON_ALREADY_EXISTS_MESSAGE = "Person with name %s already exist";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public CreateNewPersonCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String name;

        try{
            name = parameters.get(0);
        }catch (Exception e){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (engine.getAllMembers().containsKey(name)){
            throw new IllegalArgumentException(String.format(PERSON_ALREADY_EXISTS_MESSAGE,name));
        }

        Member member = factory.createMember(name);
        engine.getAllMembers().put(name,member);
        return String.format("Person with name \"%s\" was created.", name);
    }
}
