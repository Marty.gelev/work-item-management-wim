package system.wim.commands.creation;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Board;
import system.wim.models.contracts.Member;
import system.wim.models.contracts.Story;
import system.wim.models.enums.*;
import system.wim.models.modelsImpl.StoryImpl;

import java.util.List;
import java.util.NoSuchElementException;

public class CreateNewStoryInBoardCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse Story parameters";
    private final static String STORY_ALREADY_EXIST_MESSAGE = "Story %s already exist in %s board";
    private final static String BOARD_NOT_FOUND_MESSAGE = "Board not found";
    private final static String MEMBER_NOT_FOUND_MESSAGE =  "Member not found";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public CreateNewStoryInBoardCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }
    @Override
    public String execute(List<String> parameters) {
        String boardName;
        String titleOfStory;
        String description;
        Priority storyPriority;
        String memberAssigneeName;
        StorySize size;
        StoryStatus storyStatus;

        try{
            boardName = parameters.get(0);
            titleOfStory = parameters.get(1);
            description = parameters.get(2);
            storyPriority = Priority.valueOf(parameters.get(3).toUpperCase());
            memberAssigneeName = parameters.get(4);
            size = StorySize.valueOf(parameters.get(5).toUpperCase());
            storyStatus = StoryStatus.valueOf(parameters.get(6).toUpperCase());
        }catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllBoards().containsKey(boardName)){
            throw new NoSuchElementException(BOARD_NOT_FOUND_MESSAGE);
        }

        if (!engine.getAllMembers().containsKey(memberAssigneeName)){
            throw new NoSuchElementException(MEMBER_NOT_FOUND_MESSAGE);
        }

        Board board = engine.getAllBoards().get(boardName);

        if(board.getWorkItems().stream().anyMatch(x->x.getTitle().equals(titleOfStory))){
            throw new  IllegalArgumentException(String.format(STORY_ALREADY_EXIST_MESSAGE,titleOfStory,boardName));
        }

        Member member= engine.getAllMembers().get(memberAssigneeName);
        Story story = new StoryImpl(titleOfStory,description,storyPriority,member,size,storyStatus);

        engine.getAllStories().put(titleOfStory, story);
        board.addWorkItem(story);

        String message = String.format("A new story was created in %s board",boardName);
        board.addToBoardActivityHistory(message);
        engine.getAllWorkItems().put(titleOfStory,story);
        engine.getAllTasks().add(story);
        member.addToActivityHistory(message);
        return message;
    }
}
