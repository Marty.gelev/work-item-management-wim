package system.wim.commands.creation;

import system.wim.commands.contracts.*;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Board;
import system.wim.models.contracts.Bug;
import system.wim.models.contracts.Member;
import system.wim.models.enums.*;
import system.wim.models.modelsImpl.BugImpl;

import java.util.List;
import java.util.NoSuchElementException;

public class CreateNewBugInBoardCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse Bug parameters";
    private final static String BUG_ALREADY_EXIST_MESSAGE = "Bug with name %s already exist in %s board";
    private final static String BOARD_NOT_FOUND_MESSAGE = "Board not found";
    private final static String MEMBER_NOT_FOUND_MESSAGE =  "member not found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public CreateNewBugInBoardCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String boardName;
        String titleOfBug;
        String stepsToReproduce;
        String bugDescription;
        Priority bugPriority;
        String memberAssigneeName;
        BugSeverity severity;
        BugStatus bugStatus;

        try{
            boardName = parameters.get(0);
            titleOfBug = parameters.get(1);
            bugDescription = parameters.get(2);
            stepsToReproduce = parameters.get(3);
            bugPriority = Priority.valueOf(parameters.get(4).toUpperCase());
            memberAssigneeName = parameters.get(5);
            severity=BugSeverity.valueOf(parameters.get(6).toUpperCase());
            bugStatus=BugStatus.valueOf(parameters.get(7).toUpperCase());
        }catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllBoards().containsKey(boardName)){
            throw new NoSuchElementException(BOARD_NOT_FOUND_MESSAGE);
        }

        if (!engine.getAllMembers().containsKey(memberAssigneeName)){
            throw new NoSuchElementException(MEMBER_NOT_FOUND_MESSAGE);
        }

        Board board = engine.getAllBoards().get(boardName);

        if(board.getWorkItems().stream().anyMatch(x->x.getTitle().equals(titleOfBug))){
            throw new  IllegalArgumentException(String.format(BUG_ALREADY_EXIST_MESSAGE,titleOfBug,boardName));
        }

        Member member= engine.getAllMembers().get(memberAssigneeName);
        Bug bug = new BugImpl(titleOfBug,bugDescription,stepsToReproduce,bugPriority,member,severity,bugStatus);

        engine.getAllBugs().put(titleOfBug, bug);
        engine.getAllBoards().get(boardName).addWorkItem(bug);

        String message = String.format("A new bug was created in %s board",boardName);
        engine.getAllBoards().get(boardName).addToBoardActivityHistory(message);
        String memberMessage = String.format("%s was added to %s bug",memberAssigneeName,titleOfBug);
        engine.getAllMembers().get(memberAssigneeName).addToActivityHistory(memberMessage);
        engine.getAllWorkItems().put(titleOfBug,bug);
        engine.getAllTasks().add(bug);
        return message;
    }
}
