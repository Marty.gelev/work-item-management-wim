package system.wim.commands.creation;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Board;
import system.wim.models.contracts.Team;
import java.util.List;
import java.util.NoSuchElementException;

public class CreateNewBoardCommand implements Command {
    private static final String BOARD_ALREADY_EXISTS_MESSAGE = "Board with name %s already exists";
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse board parameters";
    private static final String TEAM_NOT_FOUND = "Team %s not found";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public CreateNewBoardCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String boardName;
        String teamName;

        try {
            boardName = parameters.get(0);
            teamName=parameters.get(1);
        }catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllTeams().containsKey(teamName)){
            throw new NoSuchElementException(String.format(TEAM_NOT_FOUND,teamName));
        }

        if (engine.getAllTeams().get(teamName).getBoards().containsKey(boardName)){
            throw new IllegalArgumentException(String.format(BOARD_ALREADY_EXISTS_MESSAGE,boardName));
        }

        Board board = factory.createBoard(boardName);
        engine.getAllBoards().put(boardName,board);
        Team team = engine.getAllTeams().get(teamName);
        String message = String.format("Board %s successfully added to a team %s",boardName,teamName);
        board.addToBoardActivityHistory(message);
        team.addBoard(boardName,board);
        engine.getAllTeams().get(teamName).addToTeamActivityHistory(message);

        return message;
    }
}
