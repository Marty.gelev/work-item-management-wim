package system.wim.engine.factory;

import system.wim.models.contracts.Board;
import system.wim.models.contracts.Member;
import system.wim.models.contracts.Team;

public interface WorkItemFactory {
     Team createTeam(String name);
     Member createMember(String name);
     Board createBoard(String name);
}
