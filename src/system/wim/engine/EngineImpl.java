package system.wim.engine;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.*;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.engine.providers.*;
import system.wim.models.contracts.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";

    private Reader reader;
    private Writer writer;
    private Parser parser;
    private final Map<String, Team> allTeams;
    private final Map<String, Board> allBoards;
    private final Map<String, Member> allMembers;
    private final Map<String, Bug> allBugs;
    private final Map<String, Story> allStories;
    private final Map<String, Feedback> allFeedbacks;
    private final Map<String, WorkItem> allWorkItems;
    private final List<Task> allBugsAndStoriesList;

    public EngineImpl(WorkItemFactory factory) {
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        parser = new CommandParser(factory, this);

        allTeams = new HashMap<>();
        allBoards = new HashMap<>();
        allMembers = new HashMap<>();
        allWorkItems = new HashMap<>();
        allBugs = new HashMap<>();
        allStories = new HashMap<>();
        allFeedbacks = new HashMap<>();

        allBugsAndStoriesList =  new ArrayList<>();
    }

    @Override
    public Map<String, Member> getAllMembers() {
        return allMembers;
    }

    @Override
    public Map<String, Bug> getAllBugs() {
        return allBugs;
    }

    @Override
    public Map<String, Story> getAllStories() {
        return allStories;
    }

    @Override
    public Map<String, Feedback> getAllFeedbacks() {
        return allFeedbacks;
    }

    @Override
    public Map<String, WorkItem> getAllWorkItems() {
        return allWorkItems;
    }

    @Override
    public List<Task> getAllTasks() {
        return allBugsAndStoriesList;
    }

    @Override
    public Map<String, Team> getAllTeams() {
        return allTeams;
    }

    @Override
    public Map<String, Board> getAllBoards() {
        return allBoards;
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }
        Command command = parser.parseCommand(commandAsString);
        List<String> parameters = parser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
