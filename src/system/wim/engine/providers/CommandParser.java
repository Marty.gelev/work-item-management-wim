package system.wim.engine.providers;

import system.wim.commands.addCommands.*;
import system.wim.commands.changeCommands.*;
import system.wim.commands.contracts.*;
import system.wim.commands.creation.*;
import system.wim.commands.filterCommand.*;
import system.wim.commands.showCommands.*;
import system.wim.commands.sortCommands.*;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.engine.contracts.Parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandParser implements Parser {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    private final WorkItemFactory factory;
    private final Engine engine;

    public CommandParser(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public Command parseCommand(String fullCommand) {
        String commandName = fullCommand.split(" ")[0];
        return findCommand(commandName);
    }

    @Override
    public List<String> parseParameters(String fullCommand) {
        String[] commandParts = fullCommand.split(" @ ");
        return new ArrayList<>(Arrays.asList(commandParts).subList(1, commandParts.length));
    }

    private Command findCommand(String commandName) {
        switch (commandName.toLowerCase()) {
            case "createanewperson":
                return new CreateNewPersonCommand(factory, engine);

            case "showallpeople":
                return new ShowAllPeopleCommand(factory, engine);

            case "showperson'sactivity":
                return new ShowPersonsActivityCommand(factory, engine);

            case "createanewteam":
                return new CreateNewTeamCommand(factory, engine);

            case "showallteams":
                return new ShowAllTeamsCommand(factory, engine);

            case "showteam'sactivity":
                return new ShowTeamsActivityCommand(factory, engine);

            case "addpersontoateam":
                return new AddPersonToATeamCommand(factory, engine);

            case "showallteammembers":
                return new ShowAllTeamMembersCommand(factory, engine);

            case "createanewboardinateam":
                return new CreateNewBoardCommand(factory, engine);

            case "showallteamboards":
                return new ShowAllTeamBoardsCommand(factory, engine);

            case "showboard'sactivity":
                return new ShowBoardActivityCommand(factory, engine);

            case "createanewbuginaboard":
                return new CreateNewBugInBoardCommand(factory, engine);

            case "createanewfeedbackinaboard":
                return new CreateNewFeedbackInBoardCommand(factory, engine);

            case "createanewstoryinaboard":
                return new CreateNewStoryInBoardCommand(factory, engine);

            case "changepriorityofabug":
                return new ChangePriorityOfBugCommand(factory, engine);

            case "changeseverityofabug":
                return new ChangeSeverityOfBugCommand(factory, engine);

            case "changestatusofabug":
                return new ChangeStatusOfBugCommand(factory, engine);

            case "changepriorityofastory":
                return new ChangePriorityOfStoryCommand(factory, engine);

            case "changesizeofastory":
                return new ChangeSizeOfStoryCommand(factory, engine);

            case "changestatusofastory":
                return new ChangeStatusOfStoryCommand(factory, engine);

            case "changeratingofafeedback":
                return new ChangeRatingOfFeedbackCommand(factory, engine);

            case "changestatusofafeedback":
                return new ChangeStatusOfFeedbackCommand(factory, engine);

            case "assignworkitemtoaperson":
                return new AssingWorkItemToPersonCommand(factory, engine);

            case "unassignworkitemtoaperson":
                return new UnassignWorkItemToPersonCommand(factory, engine);

                 case "addcommenttoaworkitem":
                     return new AddCommentToWorkItemCommand(factory,engine);

                 case "listall":
                     return new ListAllCommand(factory,engine);

            case "filterfeedbackonly":
                return new FilterFeedbackCommand(factory, engine);

            case "filterstoriesonly":
                return new FilterStoriesCommand(factory, engine);

            case "filterbugsonly":
                return new FilterBugsCommand(factory, engine);

            case "filterbystatus":
                return new FilterByStatusCommand(factory, engine);

            case "filterbyassignee":
                return new FilterByAssigneeCommand(factory, engine);

            case "filterbystatusandasignee":
                return new FilterByStatusAndAssigneeCommand(factory, engine);

            case "sortbypriority":
                return new SortByPriorityCommand(factory, engine);

            case "sortbyseverity":
                return new SortBySeverityCommand(factory, engine);

            case "sortbysize":
                return new SortBySizeCommand(factory, engine);

            case "sortbyrating":
                return new SortByRatingCommand(factory, engine);

            case "sortbytitle":
                return new SortByTitleCommand(factory, engine);
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandName));
    }
}
