package system.wim.engine.contracts;

import system.wim.models.contracts.*;

import java.util.List;
import java.util.Map;

public interface Engine {

    Map<String, Team> getAllTeams();

    Map<String, Board> getAllBoards();

    Map<String, Member> getAllMembers();

    Map<String, Bug> getAllBugs();

    Map<String, Story> getAllStories();

    Map<String, Feedback> getAllFeedbacks();

    Map<String,WorkItem> getAllWorkItems();

    List<Task> getAllTasks();

    void start();
}
