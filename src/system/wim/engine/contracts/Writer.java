package system.wim.engine.contracts;

public interface Writer {

    void write(String message);

    void writeLine(String message);

}
